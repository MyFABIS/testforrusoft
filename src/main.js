import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(VueRouter)

Vue.config.productionTip = false
import Users from "./components/Users.vue"
import User from "./components/User.vue"
import Adduser from "./components/Adduser.vue"
import auth from "./components/auth.vue"
Vue.component("Users", Users);
Vue.component("User", User);
Vue.component("Adduser", Adduser);
Vue.component("auth", auth);
const router = new VueRouter({
  mode:"history",
  routes: [
    {path:"/users", component: Users },
    {path:"/users/add", component: Adduser },
    {path:"/login", component: auth },
    {path:"/users/:id", component: User },
    {path:"/", redirect: "/login" },
  ],
  
})

new Vue({
  router,
  render: h => h(App),
  data(){
    return {
      items: [
        { id: 1, fio: 'Петров Петр Петрович', login: 'petr', password: '11111', email: 'petr@mail.ru',  },
        { id: 2, fio: 'Яковлев Яков Яковлевич', login: 'yaka', password: '22222', email: 'yaka@mail.ru',  },
        { id: 3, fio: 'Николаев Николай Николавич', login: 'nickola', password: '33333', email: 'nickola@mail.ru',  },
        { id: 4, fio: 'Васильев Василий Васильевич', login: 'Uasya', password: '44444', email: 'Uasya@mail.ru',  },
        { id: 5, fio: 'Степанов Степан Степанович', login: 'stephan', password: '55555', email: 'stephan@mail.ru',  },
      ],
    }
  }
}).$mount('#app')
